/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.myreportproject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gxz32
 */
public class ArtistDao implements Dao<Artist> {

    @Override
    public Artist get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artist> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist save(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist update(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artist> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ArtistReport> getArtistByTotalPrice(int limit) {
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                      select art.*, sum(ini.quantity) totalquantity,sum(ini.unitprice*ini.quantity) as totalprice from artists art
                      inner join albums alb on alb.ArtistId=art.ArtistId
                      inner join tracks tra on tra.AlbumId=alb.AlbumId
                      inner join invoice_items ini on ini.TrackId=tra.TrackId
                      inner join invoices inv on inv.InvoiceId=ini.InvoiceId
                      -- and strftime('%Y',inv.invoicedate)="2010"
                      Group by art.ArtistId
                      order by totalprice desc
                      limit ?
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtistReport obj = ArtistReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ArtistReport> getArtistByTotalPrice(String begin, String end, int limit) {
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                      select art.*, sum(ini.quantity) totalquantity,sum(ini.unitprice*ini.quantity) as totalprice from artists art
                      inner join albums alb on alb.ArtistId=art.ArtistId
                      inner join tracks tra on tra.AlbumId=alb.AlbumId
                      inner join invoice_items ini on ini.TrackId=tra.TrackId
                      inner join invoices inv on inv.InvoiceId=ini.InvoiceId
                       and inv.invoicedate between ? and ?
                      Group by art.ArtistId
                      order by totalprice desc
                      limit ?
                      """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtistReport obj = ArtistReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
